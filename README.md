# 05_Clima_app

## Clima is a location-aware weather app. 

* Can find out where you are in the world and retrieve the temperature and weather conditions.
* Can change the city at the tap of a button.

## Visual Presentation

### Preview
![Static image](documentation/screen_1.png)
![Static image](documentation/screen_2.png)
### Gif
![Gif example](https://media.giphy.com/media/AS8iSq9nEiMR7bCavE/giphy-downsized-large.gif)

## Possible improvements

* Add background pictures based on current location using Flickr API
* Add more weather info using OpenWeatherMap API, or different
* Possibility to store your favorite places (cities) to check weather info there as well

