# Clima_weather_app

## Features

This App allows you to check weather based on your current location or based on city where you are.
This app based on course ios app development from London App Brewery.

## Possible Improvements
* Add third View Controller for additional settings such as toggle between F and C degrees.
* Puting image on backgroung of main view controller based on current location of user using Flickr API.
* Adding more info from OpenWeatherMap such as Air Polution and Weather Maps.

![screen1](Documentaion/screen_1.png)
![screen2](Documentaion/screen_2.png)
